import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { ListGuestsComponent } from './components/list-guests/list-guests.component';
import { EditGuestComponent } from './components/edit-guest/edit-guest.component';
import { CreateGuestComponent } from './components/create-guest/create-guest.component';




const routes: Routes = [
  {path: 'home', component: LoginComponent},
  {path: 'listaInvitados', component: ListGuestsComponent},
  {path: 'editaInvitado/:id', component: EditGuestComponent},
  {path: 'creaInvitado', component: CreateGuestComponent},
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
