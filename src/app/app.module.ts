import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { ListGuestsComponent } from './components/list-guests/list-guests.component';
import { FormsModule } from '@angular/forms';
import localeEs from '@angular/common/locales/es';
import { registerLocaleData } from '@angular/common';
import { FiltroInvitadosPipe } from './pipes/filtro-invitados.pipe';
import { EditGuestComponent } from './components/edit-guest/edit-guest.component';
import { CreateGuestComponent } from './components/create-guest/create-guest.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';




registerLocaleData(localeEs, 'es');
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    ListGuestsComponent,
    FiltroInvitadosPipe,
    EditGuestComponent,
    CreateGuestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFontAwesomeModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
