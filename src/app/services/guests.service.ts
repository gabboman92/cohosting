import { LoginService } from './login.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class GuestsService {
// Vamos a necesitar algunos nombres y dominios para generar datos aleatorios
  private NAMES: string[] = [
    'Rhia Wheatley',
     'Frida Watkins',
     'Emmanuel Lam',
     'Savanna Hagan',
     'Georga Oneal',
     'Kaila Kerr',
     'Shayna Colley',
     'Neriah Senior',
     'Juan Gomez',
     'Addison George',
     'Keiren Garrison',
     'Riaan Owen',
     'Matas Rice',
     'Borys Emery',
     'Conna Howard',
     'Issac Cox',
     'Muna Cortez',
     'Arfa Kavanagh',
     'Keri Haas',
     'Gracey Braun',
     'Mckenzie Barrera',
     'Marley Simmons',
     'Arley Anderson',
     'Laurel Casey',
     'Diego Truong'
  ];
  private DOMAINS: string[] = [
   'gmail.com',
   'hotmail.com',
   'yandex.ru',
   'titaniumsystem.es',
   'hooli.com',
   'yahoo.com'
  ];


  private guests: Invitado[];

  constructor( private servicioLogin: LoginService,
               private router: Router) {
    // Antes de anda, revisamos si estamos logueados. Si estamos logueados todo bien, si no
    // de vuelta al login
    if (!this.servicioLogin.amILogged()) {
      // woops! parece que estás intentando entrar donde no te llaman
      this.router.navigate(['/login']);

    }

   }

   private generaInvitado(numero: number): Invitado {
    // Cogemos un nombre de la lista y
    // un dominio aleatorio
    // En principio pensaba en escoger nombres al azar pero eso causa repeticiones
    const name = this.NAMES[numero];
    const mail = name.replace( / /g, '.' ) + '@' + this.DOMAINS[Math.floor(Math.random() * this.DOMAINS.length)];
    let lastOperation = this.randomDate(new Date(2015, 0, 1), new Date());
    // status puede ser 0 o 1
    const status = Math.floor(Math.random() * 2);
    if (status == 0){
      lastOperation = null;
    }

    return {
      id: numero,
      nombre: name,
      email: mail,
      ultimaOperacion: lastOperation,
      estado: status
    };
  }

  // funcion privada util para generar fechas aleatorias entre rangos
  private randomDate(start, end) {
    return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

public getInvitados(): Invitado[] {
      // Revisamos si los invitados están en el localstorage o no.
      if (!localStorage.getItem('invitados')) {
        // Si no están los invitados los generamos

        this.guests = [];
        this.NAMES.forEach((elem, index) => {
          this.guests.push(this.generaInvitado(index));
        });

        // guardamos los invitados generados
        this.saveGuests();
      } else {
        this.guests = JSON.parse(localStorage.getItem('invitados'));
      }

      return this.guests;
}

public addGuest( guest: Invitado) {
  const newId: number = this.guests.length;
  this.saveGuests();

  const invitado: Invitado = {
    id : newId,
    nombre : guest.nombre,
    email : guest.email,
    estado : 0,
  };

  this.guests.push(invitado);


  this.saveGuests();

  this.router.navigate(['/listaInvitados']);


}


public editGuest( guest: Invitado) {

  this.guests[guest.id] = guest;
  // Guardamos en el disco la lista de invitados
  this.saveGuests();

  this.router.navigate(['/listaInvitados']);


}




private saveGuests() {
  const invitadosJson = JSON.stringify(this.guests);
  localStorage.setItem('invitados', invitadosJson.toString());
}



}

export  interface Invitado {
  id: number;
  nombre: string;
  email: string;
  ultimaOperacion?: Date;
  estado: number;
}
