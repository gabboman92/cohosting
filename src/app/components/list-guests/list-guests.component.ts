import { LoginService } from './../../services/login.service';
import { GuestsService, Invitado } from './../../services/guests.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-guests',
  templateUrl: './list-guests.component.html',
  styleUrls: ['./list-guests.component.less']
})
export class ListGuestsComponent implements OnInit {

  invitados: Invitado[];
  filtro: string;

  constructor( private servicioInvitados: GuestsService,
               private servicioLogin: LoginService,
               private router: Router) { }

  ngOnInit() {
    this.filtro = '';
    this.invitados = this.servicioInvitados.getInvitados();
  }

  editaInvitado(id: number) {
    this.router.navigate(['/editaInvitado', id.toString()]);
  }

  createGuest() {
    this.router.navigate(['/creaInvitado']);
  }

}
