import { Component, OnInit } from '@angular/core';

import { LoginService } from '../../services/login.service';
import { from } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  user: string;
  password: string;
  loginError: boolean;


  constructor( private servicioLogin: LoginService,
               private router: Router ) { }

  ngOnInit() {
    // Verificamos si estamos logueados.  Si estamos logueados vamos a la lista de invitados
    if ( this.servicioLogin.amILogged() ) {
      this.router.navigate(['/listaInvitados']);
    }
  }


  public clickLogin() {
    if ( this.servicioLogin.verificaLogin(this.user, this.password) ) {
      this.router.navigate(['/listaInvitados']);
    }
    else {
      this.loginError = true;
      setTimeout(
        function() {
          this.loginError = false;
        }, 1500);
    }
  }

}
