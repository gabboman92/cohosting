import { GuestsService } from './../../services/guests.service';
import { Component, OnInit } from '@angular/core';
import { Invitado } from 'src/app/services/guests.service';

@Component({
  selector: 'app-create-guest',
  templateUrl: './create-guest.component.html',
  styleUrls: ['./create-guest.component.less']
})
export class CreateGuestComponent implements OnInit {
  invitado: Invitado;

  constructor( private servicioInvitados: GuestsService) { }

  ngOnInit() {
    this.invitado = {
      id: null,
      nombre: '',
      email: '',
      estado: 0,
    };


  }

  createInvitado() {
    if (this.ValidateEmail(this.invitado.email)) {
      this.servicioInvitados.addGuest(this.invitado);
    }
  }

  ValidateEmail(mail: string)
{
  if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
    return (true);
  }
  alert('You have entered an invalid email address!');
  return (false);
}

}
