import { LoginService } from './../../../services/login.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})
export class NavbarComponent implements OnInit {

  constructor(private servicioLogin: LoginService) { }

  ngOnInit() {
  }

  logout(){
    this.servicioLogin.logout();
  }

}
